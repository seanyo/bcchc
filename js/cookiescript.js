/* ===================== COOKIE Start ================= */

/* Creating a cookie without an expiry date means it expires when the browser window closes*/
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

if(readCookie("locale")==null){
	if(navigator.language != null){
		createCookie("locale",navigator.language.substr(0,2),false);
	}
}
/* ===================== COOKIE End ================= */




/* ===================== Create COOKIE ================= */
//createCookie("test","true",false); //creates cookie 'test' and sets the value of 'test' to 'true' .. false is setting the no time limit of cookie




/* ===================== Read COOKIE ================= */
//readCookie("test"); //this will return the value of the cookie 'test'

$(document).ready(function(){
   
	$("#smallText").live("click", function(){
                createCookie("fontSize","small",false);
		$("body").removeClass("default");
		$("body").removeClass("large");
		$("body").addClass("small");
                return false;
	});
	$("#defaultText").live("click", function(){
                createCookie("fontSize","default",false);
		$("body").removeClass("small");
		$("body").removeClass("large");
		$("body").addClass("default");
                return false;
	});
	$("#largeText").live("click", function(){
                createCookie("fontSize","large",false);
		$("body").removeClass("small");
		$("body").removeClass("default");
		$("body").addClass("large");
                return false;
	});
        
	$("#lowContrast").live("click", function(){
                createCookie("contrast","low",false);
		$("body").removeClass("normal");
		$("body").removeClass("high");
		$("body").addClass("low");
                return false;
	});
	$("#normalContrast").live("click", function(){
                createCookie("contrast","normal",false);
		$("body").removeClass("low");
		$("body").removeClass("high");
		$("body").addClass("normal");
                return false;
	});
	$("#highContrast").live("click", function(){
                createCookie("contrast","high",false);
		$("body").removeClass("low");
		$("body").removeClass("normal");
		$("body").addClass("high");
                return false;
	});
        
        if(readCookie('fontSize')=="small"){
            $("body").addClass("small");
        }else if(readCookie('fontSize')=='default'){
            $("body").addClass("default");
        }else if(readCookie('fontSize')=='large'){
            $("body").addClass("large");
        }
        
        if(readCookie('contrast')=="low"){
            $("body").addClass("low");
        }else if(readCookie('contrast')=='normal'){
            $("body").addClass("normal");
        }else if(readCookie('contrast')=='high'){
            $("body").addClass("high");
        }
        
});